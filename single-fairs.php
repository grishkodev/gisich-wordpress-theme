<?php

get_header();

$fairs_id = 0;

?>

	<!-- main content -->
	<main role="main">
		<section class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<?php get_template_part( 'components/site', 'breadcrumbs' ); ?>
					</div>
					<div class="col-4">
						<?php get_template_part( 'components/site', 'language' ); ?>
					</div>
				</div>
				<?php
					while ( have_posts() ) : the_post();
						$fairs_id = get_the_ID();
				?>
				<div class="row">
					<div class="col-12">
						<div class="top-nav__header">
							<!-- title text -->
							<div class="text-title">
								<h1><?php the_title(); ?></h1>
								<p><?= get_post_meta(get_the_ID(), 'fairs-address', true); ?><br />
								<?= get_post_meta(get_the_ID(), 'fairs-date', true); ?></p>
							</div>
							<!-- /title text -->

							<!-- title links -->
							<div class="title-links title-links--bt">
								<ul>
									<li class="current"><a href="<?php the_permalink(); ?>"><?php pll_e('Selected works'); ?></a></li>
									<li class="hover-bottom-menu">
										<a href="#"><?php pll_e('Artists'); ?></a>
										<ul>
											<?php
												$pod = pods( 'fairs', $fairs_id );
												$left_artists = $pod->field( 'fairs-artists' );
												foreach ( $left_artists as $artist ) {
													echo '<li><a href="'.get_permalink($artist[ID]).'">'.$artist[post_title].'</a></li>';
												}
											?>
										</ul>
									</li>
									<li><a href="#" onclick="goBack()"><?php pll_e('Back'); ?></a></li>
								</ul>
							</div>
							<!-- /title links -->
							<script>
							function goBack() {
								window.history.back();
							}
							</script>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php
			$post_content = get_the_content();
			if ($post_content) :
		?>
		<section class="content">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<!-- body content -->
						<div class="content-text content-text--news">
							<?php the_content(); ?>
						</div>
						<!-- /body content -->
					</div>
				</div>
			</div>
		</section>
		<?php
			endif;
		?>
		<?php
			if( have_rows('fairs-cont') )
				$p_gallery = true;
			endwhile; // End of the loop.
			if ($p_gallery) :
		?>
	
		<section class="content content--margin-bottom">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<!-- carousel -->
						<div class="carousel carousel--second">
							<div class="carousel__body" data-flickity='{ "pageDots": false }'>

								<?php
									$post_args = array(
										'p'         => $fairs_id,
										'post_type' => 'fairs'
									);
									$post_obj = new WP_Query($post_args);

									while ( $post_obj->have_posts() ) : $post_obj->the_post();
										if( have_rows('fairs-cont') ):
											while ( have_rows('fairs-cont') ) : the_row();
												if( get_row_layout() == 'fairs-cont-img' ):
													$row_img= get_sub_field('fairs-cont-img-file');
													$row_img = $row_img['sizes']['large'];
													$row_desk = get_sub_field('fairs-cont-img-desk');
													?>
														<div class="carousel__item">
															<img src="<?= $row_img ?>" alt="">
															<div class="carousel__desk">
																<?= $row_desk ?>
															</div>
														</div>
													<?php
												endif;
												if ( get_row_layout() == 'fairs-cont-video' ) :
													$row_video = get_sub_field('fairs-cont-video-file');
													$row_video_desk = get_sub_field('fairs-cont-video-desk');
													?>
														<div class="carousel__item">
															<?= $row_video ?>
															<div class="carousel__desk">
																<?= $row_video_desk ?>
															</div>
														</div>
													<?php
												endif;
											endwhile;
										endif;

									endwhile;
								?>

							</div>
						</div>
						<!-- /carousel -->
					</div>
				</div>
			</div>
		</section>

		<?php
			endif;
		?>

	</main>
	<!-- /main content -->
</div>
<!-- /wrap -->

<?php
	get_footer();
?>