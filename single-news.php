<?php

get_header();

$news_id = 0;

?>

	<!-- main content -->
	<main role="main">
		<section class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<?php get_template_part( 'components/site', 'breadcrumbs' ); ?>
					</div>
					<div class="col-4">
						<?php get_template_part( 'components/site', 'language' ); ?>
					</div>
				</div>
				<?php
					while ( have_posts() ) : the_post();
						$news_id = get_the_ID();
				?>
				<div class="row">
					<div class="col-12">
						<div class="top-nav__header">
							<!-- title text -->
							<div class="text-title">
								<h1><?php the_title(); ?></h1>
								<h2><?= get_post_meta(get_the_ID(), 'news-where', true); ?></h2>
								<p><?= get_post_meta(get_the_ID(), 'news-date', true); ?></p>
							</div>
							<!-- /title text -->

							<!-- title links -->
							<div class="title-links">
								<ul>
									<li><a href="#" onclick="goBack()"><?php pll_e('Back'); ?></a></li>
								</ul>
							</div>
							<!-- /title links -->
							<script>
							function goBack() {
								window.history.back();
							}
							</script>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="container">
				<div class="row">
					<div class="col-6 content__news-img">
						<!-- content image -->
						<div class="content-image">
							<img src="<?php the_post_thumbnail_url(); ?>">
						</div>
						<!-- /content image -->
					</div>
					<div class="col-6 content__news-text">
						<!-- body content -->
						<div class="content-text content-text--news">
							<?php the_content(); ?>
						</div>
						<!-- /body content -->
					</div>
				</div>
				<?php
					endwhile; // End of the loop.
				?>
			</div>
		</section>
		<section class="bottom-nav">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bottom-nav__header">
							<!-- title text -->
							<div class="text-title text-title--nomargin">
								<h1><?php pll_e('Artists'); ?></h1>
							</div>
							<!-- /title text -->
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="bottom-nav__body">
							<!-- link list -->
							<div class="link-list">
								<ul>
									<?php
										$pod = pods( 'news', $news_id );
										$bottom_artists = $pod->field( 'news-artists' );
										foreach ( $bottom_artists as $artist ) {
											echo '<li><a href="'.get_permalink($artist[ID]).'">'.$artist[post_title].'</a></li>';
										}
									?>
								</ul>
							</div>
							<!-- /link list -->
						</div>
					</div>
				</div>
				<!-- <div class="row">
					<div class="col-12">
						<div class="bottom-nav__header">
							<div class="text-title text-title--nomargin">
								<h1>Публичные выставки</h1>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="bottom-nav__body">
							<div class="thumbnails-grid thumbnails-grid--news-exhibitions">

								<div class="thumbnails-grid__item">
									<a href="#">
										<div class="thumbnails-grid__img">
											<img src="../src/components/thumbnails-grid/assets/news1.jpg" alt="">
										</div>
										<div class="thumbnails-grid__title">
											DOUG AITKEN
										</div>
										<div class="thumbnails-grid__text">		
											Мираж<br />
											Палм-Спрингс, Калифорния<br />
											25 февраля - 31 октября 2017 г.
										</div>
									</a>
								</div>
								<div class="thumbnails-grid__item">
									<a href="#">
										<div class="thumbnails-grid__img">
											<img src="../src/components/thumbnails-grid/assets/news2.jpg" alt="">
										</div>
										<div class="thumbnails-grid__title">
											Киберфест
										</div>
										<div class="thumbnails-grid__text">
											Мираж<br />
											Палм-Спрингс, Калифорния<br />
											25 февраля - 31 октября 2017 г.
										</div>
									</a>
								</div>

							</div>
						</div>
					</div>
				</div> -->

			</div>
		</section>
	</main>
	<!-- /main content -->
</div>
<!-- /wrap -->

<?php
	get_footer();
?>