<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gisich
 */

add_filter('body_class','my_class_names');
function my_class_names( $classes ) {
	$classes[] = 'page';
	if ( is_front_page() )
		$classes[] = 'page--home';
	return $classes;
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="author" content="Asight.pro" />
	<meta name="viewport" content="width=device-width, user-scalable=no" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- wrap -->
	<div class="wrap">
		<?php get_template_part( 'template-parts/site', 'header' ); ?>