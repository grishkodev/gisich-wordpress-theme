<?php

get_header();

$artist_id = 0;

// routing
if (isset($_GET["series"]))
	$series = true;
elseif(isset($_GET["biography"]))
	$biography = true;
elseif(isset($_GET["public-exhibitions"]))
	$public_exhibitions = true;
elseif(isset($_GET["exhibitions"]))
	$exhibitions = true;
elseif(isset($_GET["fairs"]))
	$fairs = true;
elseif(isset($_GET["publications"]))
	$publications = true;
elseif(isset($_GET["series-preview"]) && $_GET["series-preview"] > 0)
	$series_preview = true;
else
	$series = true;

?>

<!-- main content -->
	<main role="main">
		<section class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<?php get_template_part( 'components/site', 'breadcrumbs' ); ?>
					</div>
					<div class="col-4">
						<?php get_template_part( 'components/site', 'language' ); ?>
					</div>
				</div>
				<?php
					while ( have_posts() ) : the_post();
						$artist_id = get_the_ID();
				?>
				<div class="row">
					<div class="col-12">
						<div class="top-nav__header<?php if ($series_preview) echo ' top-nav__header--without-margin'; ?>">
							<!-- title text -->
							<div class="text-title">
								<h1><?php the_title(); ?></h1>
								<div class="text-title__nav">
									<ul>
										<li class="<?php if ($series) echo 'current'; ?>"><a href="?series"><?php pll_e('Series'); ?></a></li>
										<li class="<?php if ($biography) echo 'current'; ?>"><a href="?biography"><?php pll_e('Biography'); ?></a></li>
										<li class="<?php if ($exhibitions) echo 'current'; ?>"><a href="?exhibitions"><?php pll_e('Exhibitions'); ?></a></li>
										<li class="<?php if ($public_exhibitions) echo 'current'; ?>"><a href="?public-exhibitions"><?php pll_e('Public exhibitions'); ?></a></li>
										<li class="<?php if ($fairs) echo 'current'; ?>"><a href="?fairs"><?php pll_e('Fairs'); ?></a></li>
										<li class="<?php if ($publications) echo 'current'; ?>"><a href="?publications"><?php pll_e('Publications'); ?></a></li>
									</ul>
								</div>
							</div>
							<!-- /title text -->

							<!-- title links -->
							<div class="title-links">
								<ul>
									<li><a href="#" onclick="goBack()"><?php pll_e('Back'); ?></a></li>
								</ul>
							</div>
							<!-- /title links -->
							<script>
							function goBack() {
								window.history.back();
							}
							</script>
						</div>
					</div>
				</div>
				<?php
					endwhile; // End of the loop.
				?>
				<?php if ($series_preview) : ?>
				<div class="row">
					<div class="col-12">
						<div class="top-nav__bottom">
							<!-- title links -->
							<div class="title-links">
								<ul>
									<li class="current"><a href="#"><?php echo get_the_title(intval($_GET["series-preview"])); ?></a></li>
								</ul>
							</div>
							<!-- /title links -->
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</section>

		<?php
			if ($series)
				include(locate_template('components/artist-series.php'));
			if ($biography)
				include(locate_template('components/artist-biography.php'));
			if ($series_preview)
				include(locate_template('components/artist-seriespreview.php'));
			if ($publications)
				include(locate_template('components/artist-news.php'));
			if ($public_exhibitions)
				include(locate_template('components/artist-public-exhibitions.php'));
			if ($exhibitions)
				include(locate_template('components/artist-exhibitions.php'));
			if ($fairs)
				include(locate_template('components/artist-fairs.php'));
		?>

	</main>
	<!-- /main content -->
</div>
<!-- /wrap -->

<?php
	get_footer();
?>