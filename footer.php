<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gisich
 */

?>

	<?php get_template_part( 'template-parts/site', 'footer' ); ?>
	<?php wp_footer(); ?>

</body>
</html>
