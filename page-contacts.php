<?php

/*
 * Template Name: Contacts page
 *
 */

get_header();

?>

	<!-- main content -->
	<main role="main">
		<section class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<?php get_template_part( 'components/site', 'breadcrumbs' ); ?>
					</div>
					<div class="col-4">
						<?php get_template_part( 'components/site', 'language' ); ?>
					</div>
				</div>
			</div>
		</section>
		<section class="content content--margin">
			<div class="container">
				<div class="row">
					<div class="col-7">
						<!-- content text -->
						<div class="content-text">
							<div class="content-text__map">
								<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Marina+Gisich+Gallery&amp;aq=&amp;sll=59.921216,30.339775&amp;sspn=0.048782,0.169086&amp;ie=UTF8&amp;hq=Marina+Gisich+Gallery&amp;hnear=&amp;t=m&amp;ll=59.945383,30.309906&amp;spn=0.068778,0.171318&amp;z=12&amp;iwloc=A&amp;output=embed"></iframe>
							</div>

							<?php the_field('contacts_about'); ?>

						</div>
						<!-- /content text -->
						<br /><br />
					</div>
					<div class="col-5">
						<!-- content text -->
						<div class="content-text content-text--left-padding">

							<?php the_field('contacts_info'); ?>

							<div class="content-text__img-block">
								<h4><?php pll_e('Partners'); ?></h4>
								<?php
									$partners = get_field('contacts_partners');
									if( $partners ):
										foreach( $partners as $image ):
											echo '<img src="'.$image['url'].'">';
										endforeach;
									endif;
								?>
							</div>
						</div>
						<!-- /content text -->
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<!-- carousel -->
						<div class="carousel carousel--view2">
							<div class="carousel__body" data-flickity='{ "pageDots": false }'>
								<?php
									$slider = get_field('contacts_slider');
									if( $slider ):
										foreach( $slider as $image ):
											echo '<div class="carousel__item"><img src="'.$image['url'].'"></div>';
										endforeach;
									endif;
								?>
							</div>
						</div>
						<!-- /carousel -->
					</div>
				</div>
			</div>
		</section>
	</main>
	<!-- /main content -->
</div>
<!-- /wrap -->

<?php
	get_footer();
?>