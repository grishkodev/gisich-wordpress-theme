<?php

get_header();

?>	

	<!-- main content -->
	<main role="main">
		<section class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<?php get_template_part( 'components/site', 'breadcrumbs' ); ?>
					</div>
					<div class="col-4">
						<?php get_template_part( 'components/site', 'language' ); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<!-- filters -->
						<div class="filters">
							<?php
								$cat_args = array(
									'style'              => 'list',
									'show_count'         => 0,
									'hide_empty'         => 0,
									'use_desc_for_title' => 0,
									'hierarchical'       => false,
									'number'             => NULL,
									'echo'               => 1,
									'taxonomy'           => 'news-cat',
									'title_li'           => '',
									'hide_title_if_empty' => false
								);

								echo '<ul>';
								echo '<li class="cat-item"><a href="/'.$lang.'/news/">'.pll__('See all').'</a></li>';
									wp_list_categories( $cat_args );
								echo '</ul>';
							?>
						</div>
						<!-- /filters -->
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<!-- thumbnails grid -->
						<div class="thumbnails-grid">

							<?php
								while ( have_posts() ) : the_post();
									if (get_post_meta(get_the_ID(), 'news-itspublication', true) == 0) :
									?>
									<div class="thumbnails-grid__item">
										<a href="<?php the_permalink(); ?>">
											<div class="thumbnails-grid__img">
												<img src="<?php the_post_thumbnail_url(); ?>">
											</div>
											<div class="thumbnails-grid__title">
												<?php the_title(); ?>
											</div>
											<div class="thumbnails-grid__text">
												<?= get_post_meta(get_the_ID(), 'news-where', true); ?><br />
												<?= get_post_meta(get_the_ID(), 'news-date', true); ?>
											</div>
											<div class="thumbnails-grid__more">
												<?php pll_e('View more'); ?>
											</div>
										</a>
									</div>
								<?php
									endif;
								endwhile;
							?>

						</div>
						<!-- /thumbnails grid -->
					</div>
				</div>
			</div>
		</section>
	</main>
	<!-- /main content -->

</div>
<!-- /wrap -->

<?php
	get_footer();
?>