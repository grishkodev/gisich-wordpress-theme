<?php
$footer_settings = pods( 'theme-s' );

if (pll_current_language() == 'ru'){
	$f_copyright = $footer_settings->field( 'footer_copyright_ru' );
	$f_email = $footer_settings->field( 'footer_email' );
	$f_address = $footer_settings->field( 'footer_address_ru' );
	$f_phone = $footer_settings->field( 'footer_phone' );
	$f_time = $footer_settings->field( 'footer_time_ru' );
	$f_facebook = $footer_settings->field( 'footer_facebook' );
	$f_twitter = $footer_settings->field( 'footer_twitter' );
	$f_instagram = $footer_settings->field( 'footer_instagram' );
}
else{
	$f_copyright = $footer_settings->field( 'footer_copyright_en' );
	$f_email = $footer_settings->field( 'footer_email' );
	$f_address = $footer_settings->field( 'footer_address_en' );
	$f_phone = $footer_settings->field( 'footer_phone' );
	$f_time = $footer_settings->field( 'footer_time_en' );
	$f_facebook = $footer_settings->field( 'footer_facebook' );
	$f_twitter = $footer_settings->field( 'footer_twitter' );
	$f_instagram = $footer_settings->field( 'footer_instagram' );
}
?>
<!-- footer -->
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="footer__left">
					<!-- copyright -->
					<div class="copyright">
						<?= $f_copyright ?>
					</div>
					<!-- /copyright -->
					<!-- follow use -->
					<div class="follow-us">
						<ul>
							<?php if ($f_facebook){ ?>
							<li class="follow-us__facebook"><a href="<?= $f_facebook ?>" title="Facebook"></a></li>
							<?php } ?> 
							<?php if ($f_twitter){ ?>
							<li class="follow-us__twitter"><a href="<?= $f_twitter ?>" title="Twitter"></a></li>
							<?php } ?>
							<?php if ($f_instagram){ ?>
							<li class="follow-us__instagram"><a href="<?= $f_instagram ?>" title="Instagram"></a></li>
							<?php } ?>
						</ul>
					</div>
					<!-- /follow us -->
				</div>
				<div class="footer__right">
					<!-- contacts -->
					<div class="footer-contacts">
						<div class="footer-contacts__block">
							<a href="mailto:<?= $f_email ?>" title="E-mail"><?= $f_email ?></a>
						</div>
						<div class="footer-contacts__block">
							<?= $f_address ?>
						</div>
						<div class="footer-contacts__block">
							<?= $f_phone ?>
						</div>
						<div class="footer-contacts__block">
							<?= $f_time ?>
						</div>
					</div>
					<!-- /contacts -->
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- /footer -->

