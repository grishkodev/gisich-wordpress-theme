<!-- header -->
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col-4">
				<!-- logo -->
				<div class="logo">
					<a href="<?= get_home_url() ?>">MARINA GISICH GALLERY</a>
				</div>
				<!-- /logo -->
			</div>
			<div class="col-8">
				<!-- main menu -->
				<div class="menu-mob"><?php pll_e('Menu'); ?></div>
				<nav role="navigation" class="top-level-menu">
					<?php wp_nav_menu(array(
						'theme_location' => 'menu-1',
						'menu'            => '', 
						'container'       => '', 
						'container_class' => '', 
						'container_id'    => '',
						'menu_class'      => '', 
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => '',
						'before'          => '',
						'after'           => '',
						'link_before'     => '<span>',
						'link_after'      => '</span>',
						'items_wrap'      => '<ul class="clearfix">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					));
					?>
				</nav>
				<!-- /main menu -->
			</div>
		</div>
		<?php if ( is_front_page() ) { ?>
		<div class="row">
			<div class="col-12">
				<div class="header__lang">
					<?php get_template_part( 'components/site', 'language' ); ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</header>
<!-- /header -->