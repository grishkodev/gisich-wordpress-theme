<?php

get_header();

?>

	<!-- main content -->
	<main role="main">
		<section class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<?php get_template_part( 'components/site', 'breadcrumbs' ); ?>
					</div>
					<div class="col-4">
						<?php get_template_part( 'components/site', 'language' ); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="top-nav__header">
							<!-- title text -->
							<div class="text-title">
								<h1><?php pll_e('Artists'); ?></h1>
							</div>
							<!-- /title text -->

							<!-- change view -->
							<div class="change-view">
								<a href="?view=grid" class="change-view__grid">Grid</a>
								<a href="?view=list" class="change-view__list">List</a>
							</div>
							<!-- /change view -->
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<?php
				if (isset($_GET["view"]) && $_GET["view"] == "grid")
					get_template_part( 'components/artists', 'grid' );
				elseif(isset($_GET["view"]) && $_GET["view"] == "list")
					get_template_part( 'components/artists', 'list' );
				else
					get_template_part( 'components/artists', 'list' );
			?>
		</section>
	</main>
	<!-- /main content -->


</div>
<!-- /wrap -->

<?php
	get_footer();
?>