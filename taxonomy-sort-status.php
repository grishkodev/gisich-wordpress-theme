<?php

get_header();

$catagory_id = get_queried_object()->term_id;

$current = false;
$past = false;
if ($catagory_id == 79 || $catagory_id == 81)
	$current = true;
if ($catagory_id == 75 || $catagory_id == 77)
	$past = true;

$public_value = 0;

if (isset($_GET["public"])){
	$public_cat = true;
	$public_value = 1;
}

?>	

	<!-- main content -->
	<main role="main">
		<section class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<?php get_template_part( 'components/site', 'breadcrumbs' ); ?>
					</div>
					<div class="col-4">
						<?php get_template_part( 'components/site', 'language' ); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<!-- filters -->
						<div class="filters<?php if ($current) echo ' filters--first'; ?>">
							<?php
								$cat_args = array(
									'style'              => 'list',
									'show_count'         => 0,
									'hide_empty'         => 0,
									'use_desc_for_title' => 0,
									'hierarchical'       => false,
									'number'             => NULL,
									'echo'               => 1,
									'taxonomy'           => 'sort-status',
									'title_li'           => '',
									'hide_title_if_empty' => false,
									'orderby'            => 'id'
								);

								echo '<ul>';
								echo '<li class="cat-item"><a href="/'.$lang.'/exhibitions/">'.pll__('See all').'</a></li>';
									wp_list_categories( $cat_args );
								echo '</ul>';
							?>
						</div>
						<?php if ($past) : ?>
						<div class="filters filters--second">
							<?php
								$cat2_args = array(
									'style'              => 'list',
									'show_count'         => 0,
									'hide_empty'         => 0,
									'use_desc_for_title' => 0,
									'hierarchical'       => false,
									'number'             => NULL,
									'echo'               => 1,
									'taxonomy'           => 'sort-years',
									'title_li'           => '',
									'hide_title_if_empty' => false,
									'orderby'            => 'id'
								);

								echo '<ul>';
								echo '<li class="cat-item current-cat"><a href="/'.$lang.'/exhibitions/">'.pll__('See all').'</a></li>';
									wp_list_categories( $cat2_args );
								echo '</ul>';
							?>
						</div>
						<?php endif; ?>
						<!-- /filters -->
					</div>
				</div>
			</div>
		</section>
		<?php
			if ($public_cat)
				echo '<script>var anchor = document.getElementsByClassName("cat-item"); for(var i = 0; i < anchor.length; i++){ anchor[i].childNodes[0].href += "?public"; }</script>'
		?>
		<section class="content">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<!-- thumbnails grid -->
						<div class="thumbnails-grid<?php if ($current) echo ' thumbnails-grid--full'; ?>">

							<?php
								$args = array(
									'post_type' => 'exhibitions',
									'posts_per_page' => 100,
									'tax_query' => array(
										array(
											'taxonomy' => 'sort-status',
											'field'    => 'id',
											'terms'    => $catagory_id
										)
									),
									'meta_query' => array(
		                                'relation' => 'OR',
		                                array(
		                                    'key' => "ex-publ",
		                                    'value' => $public_value,
		                                )
		                            )
								);
								$loop = new WP_Query( $args );

								while ( $loop->have_posts() ) : $loop->the_post();

								?>
									<div class="thumbnails-grid__item">
										<a href="<?php the_permalink(); ?>">
											<div class="thumbnails-grid__img">
												<img src="<?php the_post_thumbnail_url(); ?>">
											</div>
											<div class="thumbnails-grid__title">
												<?php the_title(); ?>
											</div>
											<div class="thumbnails-grid__text">
												<?= get_post_meta(get_the_ID(), 'ex-date', true); ?>
											</div>
										</a>
									</div>
								<?php

								endwhile;
							?>

						</div>
						<!-- /thumbnails grid -->
					</div>
				</div>
			</div>
		</section>
	</main>
	<!-- /main content -->

</div>
<!-- /wrap -->

<?php

	get_footer();
?>