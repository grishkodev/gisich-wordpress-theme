<section class="content content--margin-bottom">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- carousel -->
				<div class="carousel carousel--second">
					<div class="carousel__body" data-flickity='{ "pageDots": false }'>

						<?php
							$series_preview_id = intval($_GET["series-preview"]);
							$post_args = array(
								'p'         => $series_preview_id,
								'post_type' => 'series',
								'posts_per_page' => 100
							);
							$post_obj = new WP_Query($post_args);

							while ( $post_obj->have_posts() ) : $post_obj->the_post();
								if( have_rows('series-cont') ):
									while ( have_rows('series-cont') ) : the_row();
										if( get_row_layout() == 'series-cont-img' ):
											$row_img= get_sub_field('series-cont-img-file');
											$row_img = $row_img['sizes']['large'];
											$row_desk = get_sub_field('series-cont-img-desk');
											?>
												<div class="carousel__item">
													<img src="<?= $row_img ?>" alt="">
													<div class="carousel__desk">
														<?= $row_desk ?>
													</div>
												</div>
											<?php
										endif;
										if ( get_row_layout() == 'series-cont-video' ) :
											$row_video = get_sub_field('series-cont-video-file');
											$row_video_desk = get_sub_field('series-cont-video-desk');
											?>
												<div class="carousel__item">
													<?= $row_video ?>
													<div class="carousel__desk">
														<?= $row_video_desk ?>
													</div>
												</div>
											<?php
										endif;
									endwhile;
								endif;

							endwhile;
						?>

					</div>
				</div>
				<!-- /carousel -->
			</div>
		</div>
	</div>
</section>