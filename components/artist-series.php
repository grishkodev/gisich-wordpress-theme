<!-- series -->
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- thumbnails grid -->
				<div class="thumbnails-grid">
					<?php
						$series_query = new WP_Query( array(
						    'post_type' => 'series',
						    'posts_per_page' => 100,
						    'meta_query' => array(
                                'relation' => 'OR',
                                array(
                                    'key' => "series-author",
                                    'value' => $artist_id
                                )
                            )
						) );
						if ( $series_query-> have_posts() ) { while ( $series_query->have_posts() ) {
						    $series_query->the_post();
						    ?>
							    <div class="thumbnails-grid__item">
									<a href="?series-preview=<?php the_id(); ?>">
										<div class="thumbnails-grid__img">
											<img src="<?php the_post_thumbnail_url(); ?>">
										</div>
										<div class="thumbnails-grid__title">
											<?php the_title(); ?>
										</div>
									</a>
								</div>
						    <?php
						} }
					?>

				</div>
				<!-- /thumbnails grid -->
			</div>
		</div>
	</div>
</section>
<!-- /series -->