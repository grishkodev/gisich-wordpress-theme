<section class="content content--margin-bottom">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- thumbnails grid -->
				<div class="thumbnails-grid">
					
					<?php
						$args = array( 
							'post_type' => 'news',
							'posts_per_page' => 100,
							'meta_query' => array(
                                'relation' => 'AND',
                                array(
                                    'key' => "news-artists",
                                    'value' => $artist_id,
                                ),
                                array(
                                    'key' => "news-itspublication",
                                    'value' => 1
                                )
                            )
						);
						$loop = new WP_Query( $args );

						while ( $loop->have_posts() ) : $loop->the_post();

						?>
							<div class="thumbnails-grid__item">
								<a href="<?php the_permalink(); ?>">
									<div class="thumbnails-grid__img">
										<img src="<?php the_post_thumbnail_url(); ?>">
									</div>
									<div class="thumbnails-grid__title">
										<?php the_title(); ?>
									</div>
									<div class="thumbnails-grid__text">
										<?= get_post_meta(get_the_ID(), 'news-where', true); ?><br />
										<?= get_post_meta(get_the_ID(), 'news-date', true); ?>
									</div>
									<div class="thumbnails-grid__more">
										<?php pll_e('View more'); ?>
									</div>
								</a>
							</div>
						<?php

						endwhile;
					?>

				</div>
				<!-- /thumbnails grid -->
			</div>
		</div>
	</div>
</section>