<!-- items list -->
<?php if ( is_front_page() ) {
?>
<div class="items-list">
<?php
}else{
?>
<div class="items-list items-list--artists">
<?php
}
?>
	<div class="container">
		<div class="row">
			<?php
				$args = array( 'post_type' => 'artists', 'order' => 'ASC', 'posts_per_page' => 100 );
				$loop = new WP_Query( $args );
				$posts_num = $loop->post_count;
				$post_col_num = round($post_num / 4 + 1);
				$artists_arr = [];
				$i = 0;

				while ( $loop->have_posts() ) : $loop->the_post();

					$artists_arr[$i] = '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
					$i++;

				endwhile;
				$artists_arr_v = array_chunk($artists_arr, $post_col_num);
				foreach ( $artists_arr_v as $artist_list ) {
					echo '<div class="col-3"><ul>';
					foreach ( $artist_list as $artist ) {
						echo $artist;
					}
					echo '</ul></div>';
				}
			?>
		</div>
	</div>
</div>
<!-- /items list -->