<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- thumbnails grid -->
				<div class="thumbnails-grid">

					<?php
						$args = array(
							'post_type' => 'exhibitions',
							'posts_per_page' => 100,
							'meta_query' => array(
                                'relation' => 'AND',
                                array(
                                    'key' => "ex-publ",
                                    'value' => 0
                                ),
                                array(
                                    'key' => "ex-artist",
                                    'value' => $artist_id
                                )
                            )
						);
						$loop = new WP_Query( $args );

						while ( $loop->have_posts() ) : $loop->the_post();

						?>
							<div class="thumbnails-grid__item">
								<a href="<?php the_permalink(); ?>">
									<div class="thumbnails-grid__img">
										<img src="<?php the_post_thumbnail_url(); ?>">
									</div>
									<div class="thumbnails-grid__title">
										<?php the_title(); ?>
									</div>
									<div class="thumbnails-grid__text">
										<?= get_post_meta(get_the_ID(), 'ex-date', true); ?>
									</div>
								</a>
							</div>
						<?php

						endwhile;
					?>

				</div>
				<!-- /thumbnails grid -->
			</div>
		</div>
	</div>
</section>