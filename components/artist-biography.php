<!-- bio -->
<section class="content content--margin-bottom">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- body content -->
				<div class="content-text">

					<?= get_post_meta($artist_id, 'artist-page-bio', true); ?>

				</div>
				<!-- /body content -->
			</div>
		</div>
	</div>
</section>
<!-- /bio -->