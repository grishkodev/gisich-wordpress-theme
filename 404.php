<?php

get_header();

?>

	<!-- main content -->
	<main role="main">
		<section class="not-found">
			<div class="container">
				<?php if (pll_current_language() == 'en'){ ?>
				<div class="col-12">
					<h2>404</h2>
					<h3>Page not found</h3>
					<a href="<?= get_home_url() ?>">Go home</a>
				</div>
				<?php }else{ ?>
				<div class="col-12">
					<h2>404</h2>
					<h3>Страница не найдена</h3>
					<a href="<?= get_home_url() ?>">Вернуться на главную</a>
				</div>
				<?php } ?>
			</div>
		</section>
	</main>
	<!-- /main content -->

</div>
<!-- /wrap -->

<?php
	get_footer();
?>