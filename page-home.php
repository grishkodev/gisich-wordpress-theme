<?php

/*
 * Template Name: Home page
 *
 */

get_header();

?>

</div>
<!-- /wrap -->

<!-- full banner -->
<a href="<?php the_field('artist_link'); ?>" class="full-img">
	<div class="full-img__text">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="full-img__text-body">
						<h2><?php the_field('artist_name'); ?></h2>
						<?php the_field('artist_event'); ?><br />
						<?php the_field('artist_date'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="#" class="full-img__more"><?php pll_e('View more'); ?></a>
</a>

<?php
$images = get_field('artist_background');

if( $images ):
	$i = true;
	foreach( $images as $image ):
		if ($i)
			$img_url .= '"'.$image['url'].'"';
		else
			$img_url .= ',"'.$image['url'].'"';
		$i = false;
	endforeach;
	?>
		<script>
		var imageArray = [
			<?= $img_url ?>
		]
		</script>
	<?php
endif;
?>
<!-- /full banner -->

<?php get_template_part( 'components/artists', 'list' ); ?>

<?php
	get_footer();
?>